##### Import Libraries Started ##############
import sqlite3
from sqlite3 import Error
import csv
import time
from os import path

home = path.expanduser('~')

### create and connect with database ####
def sql_connection():
    try:

        con = sqlite3.connect(path.join(home,'testdatabase.db'))
        print("Connection is established: Database is created ")
        return con

    except Error:
        print(Error)

def read_csvmethod1(con):
    cursorObj = con.cursor()
    cursorObj.execute('''CREATE TABLE IF NOT EXISTS TEST
                 (id INTEGER PRIMARY KEY,
                  timestamp TIMESTAMP NOT NULL,
                  temperature REAL NOT NULL,
                   duration REAL NOT NULL )''')

    cursorObj.execute('''CREATE TABLE IF NOT EXISTS LOG
                 (id INTEGER PRIMARY KEY ,
                 url TEXT NOT NULL,
                 request_type TEXT NOT NULL,    
                  created TIMESTAMP NOT NULL
                  
                  )''')
    # Opening and reading csv file
    with open(home+'/task_data.csv', mode='r') as csv_file:
        start_time_iterate = time.time()
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        ## Iterate Each row ###
        csvData =[]
        for row in csv_reader:
            csvData.append(row)

        cursorObj.execute('Delete FROM TEST')
        cursorObj.executemany('INSERT INTO TEST VALUES (?,?,?,?);',csvData)

        con.commit()
        print("time to complete in read_csvmethod1 ", time.time() - start_time_iterate)



def read_csvmethod2(con):
    cursorObj = con.cursor()
    cursorObj.execute('Delete FROM TEST')
    # Opening and reading csv file
    with open(home+'/task_data.csv', mode='r') as csv_file:
        start_time_iterate = time.time()
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        ## Iterate Each row ###
        for row in csv_reader:
            cursorObj.execute('INSERT INTO TEST VALUES (?,?,?,?);',row)

        con.commit()
        print("time to complete in read_csvmethod2", time.time() - start_time_iterate)


def yield_csvmethod():
    # Opening and reading csv file
    with open(home+'/task_data.csv', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        for row in csv_reader:
            yield row



if __name__ == '__main__':

    con = sql_connection()
    read_csvmethod1(con)
    read_csvmethod2(con)

    ### Read Csv 3 method started using generator ###
    gen = yield_csvmethod()
    cursorObj = con.cursor()
    cursorObj.execute('Delete FROM TEST')
    genList = []
    start_time_iterateyieldmethod = time.time()
    for x in gen:
        cursorObj.execute('INSERT INTO TEST VALUES (?,?,?,?);', x)

    con.commit()
    print("time to complete in yield method", time.time() - start_time_iterateyieldmethod)
    con.close()




